\begin{thebibliography}{10}

\bibitem{Adams}
A.~Adams, B.~Gomez, C.~Jones, and M.~Zhang.
\newblock {Optimizing the flow in multicommodity networks with parametric link
  capacity}.
\newblock {\em Operations Research}, 64(2):200--219, 2016.

\bibitem{AhujaEtal}
R.K. Ahuja, T.L. Magnanti, and J.B. Orlin.
\newblock {\em Network Flows: Theory, Algorithms, and Applications}.
\newblock Prentice Hall, Upper Saddle River, NJ, 1993.

\bibitem{Goemans}
M.X. Goemans.
\newblock Improved approximation algorithms for scheduling with release dates.
\newblock In {\em Proceedings of the 8th Annual ACM-SIAM Symposium on Discrete
  Algorithms}, pages 591--598, 1997.

\bibitem{Jones}
J.C. Jones.
\newblock The price of anarchy in social networks.
\newblock Working Paper~79, Business School, University of Maryland, College
  Park, MD, 2005.

\bibitem{Ponza}
A.~Ponza.
\newblock Optimization of drone-assisted parcel delivery.
\newblock Master's thesis, Univ. Degli Studi Di Padova, University of Padova,
  Italy, 2016.

\bibitem{ReadTutte}
R.C. Read and W.T. Tutte.
\newblock Chromatic polynomials.
\newblock In L.W. Beineke and R.J. Wilson, editors, {\em Selected Topics in
  Graph Theory III}, pages 15--42. Academic Press, New York, 1988.

\bibitem{Seibert}
S.~Seibert and A.~Unger.
\newblock A 1.5-approximation of the minimal manhattan network.
\newblock In {\em 16th International Symposium on Algorithms and Computation},
  pages 246--255, 2005.

\bibitem{Shapiro}
A.~Shapiro.
\newblock Monte {C}arlo sampling methods.
\newblock In A.~Ruszczy\'{n}ski and A.~Shapiro, editors, {\em Stochastic
  Programming}, pages 353--425. North-Holland Publishing Company, Amsterdam,
  2003.

\bibitem{vonNeumann}
J.~von Neumann and O.~Morgenstern.
\newblock {\em Theory of Games and Economic Behavior}.
\newblock Princeton University Press, Princeton, NJ, 1953.

\bibitem{Wilson}
A.R. Wilson.
\newblock {\em Exact and Heuristic Algorithms for Cut Scheduling Problems}.
\newblock PhD thesis, Department of Systems and Industrial Engineering, The
  University of Arizona, Tucson, AZ, 2010.

\end{thebibliography}
